# DBRequestExecuter

## Тестовое задание: 
Написать консольное приложение. 
Приложение по команде считывает из файла input.txt sql запрос, выполняет его и записывает результат выполнения в файл output.csv. 
Возникающие ошибки выводятся на экран консоли и не завершают выполнение программы. 
Предусмотреть возможность подключения к базе данных Microsoft SQL Server через конфиг.

Для создания приложения использовать C# .net. framework. 
Для отладки Microsoft Visual Studio.
В виде БД желательно использовать локальную базу данных mdf.
БД должна находиться в файле (она не должна зависеть от внешних путей).
