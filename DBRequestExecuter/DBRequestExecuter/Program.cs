﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using System.Text;
using System.Data;

namespace DBRequestExecuter
{
    class Program
    {
        private static IConfiguration _iConfiguration;
        static void Main(string[] args)
        {
            GetAppSettingsFile();
            try
            {
                ScriptExecute();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка - {ex.Message}" + Environment.NewLine +
                    $"Stack - {ex.StackTrace}");
            }

            Console.WriteLine("Press any key to stop.");
            Console.ReadKey();
        }

        static void GetAppSettingsFile()
        {
            var builder = new ConfigurationBuilder()
                                 .SetBasePath(Directory.GetCurrentDirectory())
                                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            _iConfiguration = builder.Build();
        }

        static void ScriptExecute()
        {

            string sqlConnectionString = _iConfiguration.GetConnectionString("Default");
            var path = Directory.GetCurrentDirectory();
            string inputfilePath = $"{path}\\input.txt";
            string outputfilePath = $"{path}\\output.csv";

            string script = File.ReadAllText(inputfilePath);

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            using (SqlCommand cmd = new SqlCommand(script, conn))            
            using (StreamWriter outputFile = new StreamWriter(outputfilePath))
            {                
                cmd.CommandType = CommandType.Text;
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                StringBuilder outputData = new StringBuilder();

                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string value = reader[i].ToString();
                        if (value.Contains(","))
                            value = $"\"{value}\"";

                        outputData.Append(value.Replace(Environment.NewLine, " ") + ";");
                    }
                    outputData.Length--; // Remove the last comma
                    outputData.AppendLine();
                }                
                outputFile.Write(outputData.ToString());
            }
        }
    }
}